{{/*
Expand the name of the chart.
*/}}
{{- define "django.name" -}}
{{- default .Chart.Name .Values.django.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "django.fullname" -}}
{{- if .Values.django.fullnameOverride }}
{{- .Values.django.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.django.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "django.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "django.labels" -}}
helm.sh/chart: {{ include "django.chart" . }}
{{ include "django.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "django.selectorLabels" -}}
app.kubernetes.io/name: {{ include "django.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "django.serviceAccountName" -}}
{{- if .Values.django.serviceAccount.create }}
{{- default (include "django.fullname" .) .Values.django.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.django.serviceAccount.name }}
{{- end }}
{{- end }}

{{- define "imagePullSecret" }}
{{- with .Values.imageCredentials }}
{{- printf "{\"auths\":{\"%s\":{\"username\":\"%s\",\"password\":\"%s\",\"email\":\"%s\",\"auth\":\"%s\"}}}" .registry .username .password .email (printf "%s:%s" .username .password | b64enc) | b64enc }}
{{- end }}
{{- end }}

{{/*
CELERY WORKER
*/}}

{{- define "django-celeryworker.name" -}}
{{- default .Chart.Name .Values.djangoCeleryworker.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "django-celeryworker.fullname" -}}
{{- if .Values.djangoCeleryworker.fullnameOverride }}
{{- .Values.djangoCeleryworker.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.djangoCeleryworker.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{- define "django-celeryworker.labels" -}}
helm.sh/chart: {{ include "django.chart" . }}
{{ include "django-celeryworker.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}


{{- define "django-celeryworker.selectorLabels" -}}
app.kubernetes.io/name: {{ include "django.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "django-celeryworker.serviceAccountName" -}}
{{- if .Values.djangoCeleryworker.serviceAccount.create }}
{{- default (include "django-celeryworker.fullname" .) .Values.djangoCeleryworker.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.djangoCeleryworker.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
CELERY BEAT
*/}}
{{- define "django-celerybeat.name" -}}
{{- default .Chart.Name .Values.djangoCelerybeat.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "django-celerybeat.fullname" -}}
{{- if .Values.djangoCelerybeat.fullnameOverride }}
{{- .Values.djangoCelerybeat.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.djangoCelerybeat.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{- define "django-celerybeat.labels" -}}
helm.sh/chart: {{ include "django.chart" . }}
{{ include "django-celerybeat.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{- define "django-celerybeat.selectorLabels" -}}
app.kubernetes.io/name: {{ include "django-celerybeat.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "django-celerybeat.serviceAccountName" -}}
{{- if .Values.djangoCelerybeat.serviceAccount.create }}
{{- default (include "django-celerybeat.fullname" .) .Values.djangoCelerybeat.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.djangoCelerybeat.serviceAccount.name }}
{{- end }}
{{- end }}
