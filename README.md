Simple helm chart to install a django app

- Install 

```bash
helm repo add --username <username> --password <personal_access_token> django-helm https://gitlab.com/api/v4/projects//packages/helm/stable
helm install my-release project-1/mychart
```
